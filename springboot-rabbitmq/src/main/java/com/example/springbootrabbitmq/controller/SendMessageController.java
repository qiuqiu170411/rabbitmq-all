package com.example.springbootrabbitmq.controller;

import com.example.springbootrabbitmq.enums.MqEnums;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

/**
 * @author wwp
 * @version 1.0.0
 * @since 2021年07月19日 14:32:00
 */
@Slf4j
@RestController
@Tag(name = "send-message-controller", description = "发送消息API")
public class SendMessageController {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @GetMapping("/ttl/{str}")
    @Operation(description = "发送消息到ttl队列", summary = "发送消息到ttl队列", tags = "send-message-controller")
    public void sendMessage(@PathVariable(value = "str") String str) {
        log.info(">>> 当前时间:[{}],发送一条信息给两个 TTL 队列:[{}]", LocalDateTime.now(), str);
        rabbitTemplate.convertAndSend(MqEnums.NORMAL_A.getExchange(), MqEnums.NORMAL_A.getRoutingKey(), "消息来自 ttl 为 10S 的队列: " + str);
        rabbitTemplate.convertAndSend(MqEnums.NORMAL_B.getExchange(), MqEnums.NORMAL_B.getRoutingKey(), "消息来自 ttl 为 40S 的队列: " + str);
    }

    @Operation(description = "发送ttl消息", summary = "发送ttl消息", tags = "send-message-controller")
    @GetMapping("sendExpirationMsg/{message}/{ttlTime}")
    public void sendMsg(@PathVariable(value = "message") String message, @PathVariable(value = "ttlTime") String ttlTime) {
        rabbitTemplate.convertAndSend(MqEnums.NORMAL_C.getExchange(), MqEnums.NORMAL_C.getRoutingKey(), message, correlationData -> {
            correlationData.getMessageProperties().setExpiration(ttlTime);
            return correlationData;
        });
        log.info(">>> 当前时间:[{}],发送一条时长:[{}]毫秒TTL,信息[{}]给队列!", LocalDateTime.now(), ttlTime, message);
    }

    @GetMapping("sendMsg/{message}/{delayTime}")
    @Operation(description = "发送ttl消息(结合延迟队列插件)", summary = "发送ttl消息(结合延迟队列插件)", tags = "send-message-controller")
    public void sendMsgToDelayed(@PathVariable(value = "message") String message, @PathVariable(value = "delayTime") Integer delayTime) {
        rabbitTemplate.convertAndSend(MqEnums.DELAYED.getExchange(), MqEnums.DELAYED.getRoutingKey(), message, correlationData -> {
            correlationData.getMessageProperties().setDelay(delayTime);
            return correlationData;
        });
        log.info(">>> 当前时间:[{}],发送一条时长:[{}]毫秒TTL,信息[{}]给延迟队列(插件实现)!", LocalDateTime.now(), delayTime, message);
    }
}
