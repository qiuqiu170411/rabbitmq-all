package com.example.springbootrabbitmq.controller;

import com.example.springbootrabbitmq.enums.MqEnums;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

/**
 * @author wwp
 * @version 1.0.0
 * @since 2021年07月20日 15:49:00
 */
@Slf4j
@RestController
@RequestMapping("/backup")
@Tag(name = "backup-controller", description = "备份交换机API")
public class BackupController {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @GetMapping("sendMessage/{message}/{type}")
    @Operation(description = "发送消息模拟接收回调", summary = "发送消息模拟接收回调", tags = "backup-controller")
    public void sendMessage(@PathVariable String message, @PathVariable Integer type) {
        //第一个消息会被接收 第二个消息没有队列接收 会进入到报警队列 和 备份队列中去
        if (type == 1) {
            rabbitTemplate.convertAndSend(MqEnums.TEST.getExchange(), MqEnums.TEST.getRoutingKey(), message, new CorrelationData(UUID.randomUUID().toString()));
        } else {
            // 发送没法路由的消息
            rabbitTemplate.convertAndSend(MqEnums.TEST.getExchange(), "key2", message, new CorrelationData(UUID.randomUUID().toString()));
        }
    }
}
