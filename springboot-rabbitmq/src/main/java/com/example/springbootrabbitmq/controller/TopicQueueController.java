package com.example.springbootrabbitmq.controller;

import com.example.springbootrabbitmq.enums.MqEnums;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

/**
 * @author wwp
 * @version 1.0.0
 * @since 2021年07月20日 15:49:00
 */
@Slf4j
@RestController
@RequestMapping("/topic")
@Tag(name = "topic-controller", description = "topic交换机API")
public class TopicQueueController {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private AmqpTemplate amqpTemplate;

    @GetMapping("sendMessage/{message}/{type}")
    @Operation(description = "发送消息模拟接收回调", summary = "发送消息模拟接收回调", tags = "topic-controller")
    public void sendMessage(@PathVariable String message, @PathVariable Integer type) {
        rabbitTemplate.convertAndSend(MqEnums.TOPIC_QUEUE.getExchange(), MqEnums.TOPIC_QUEUE.getRoutingKey(), message, new CorrelationData(UUID.randomUUID().toString()));
//        amqpTemplate.convertAndSend(MqEnums.TOPIC_QUEUE.getExchange(), "",message);
    }
}
