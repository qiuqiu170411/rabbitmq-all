package com.example.springbootrabbitmq.controller;

import com.example.springbootrabbitmq.enums.MqEnums;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

/**
 * @author wwp
 * @version 1.0.0
 * @since 2021年07月20日 09:54:00
 */
@Slf4j
@RestController
@RequestMapping("/confirm")
@Tag(name = "confirm-controller", description = "发布确认模式API")
public class ConfirmController {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @GetMapping("sendMessage/{message}")
    @Operation(description = "发送消息模拟接收回调", summary = "发送消息模拟接收回调", tags = "confirm-controller")
    public void sendMessage(@PathVariable String message) {
        //第一个消息会被接收 第二个消息没有队列接收

        String routingKey = MqEnums.CONFIRM.getRoutingKey();
        rabbitTemplate.convertAndSend(MqEnums.CONFIRM.getExchange(), routingKey, message + routingKey, new CorrelationData(UUID.randomUUID().toString()));

        routingKey = "key2";
        rabbitTemplate.convertAndSend(MqEnums.CONFIRM.getExchange(), routingKey, message + routingKey, new CorrelationData(UUID.randomUUID().toString()));
    }

}
