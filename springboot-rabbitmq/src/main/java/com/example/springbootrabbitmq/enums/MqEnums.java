package com.example.springbootrabbitmq.enums;

import lombok.Getter;

/**
 * @author wwp
 * @version 1.0.0
 * @since 2021年07月02日 09:26:00
 */
@Getter
public enum MqEnums {

    /**
     * 队列A、B 都是延迟队列 队列中的消息 到期会过期转入到死信队列
     * 队列C 不是延迟队列 但是发送到队列C中的消息 是过期消息 则到期无人消费 也会转入到死信队列
     */
    NORMAL_A("normal-queue-a", "normal-exchange", "normal-a-routingKey"),
    NORMAL_B("normal-queue-b", "normal-exchange", "normal-b-routingKey"),
    NORMAL_C("normal-queue-c", "normal-exchange", "normal-c-routingKey"),
    /**
     * 发布确认模式队列
     */
    CONFIRM("confirm.queue", "confirm.exchange", "confirm.routingKey"),
    /**
     * 备份队列 当消息投递给交换机时投递失败 消息转入到备份交换机中 备份交换机将消息 分发给backup队列 和 warning 报警队列
     */
    ALTERNATE("backup.queue", "backup.exchange", "backup.routingKey"),
    WARNING("warning.queue", "backup.exchange", "warning.routingKey"),
    TEST("test.queue", "test.exchange", "test.routingKey"),

    /**
     * 延迟队列(插件实现)
     */
    DELAYED("delayed.queue", "delayed.exchange", "delayed.routingKey"),

    /**
     * 死信队列
     */
    DEAD_LETTER("dead-letter-queue", "dead-letter-exchange", "dead-letter-routingKey"),

    TOPIC_QUEUE("topic-queue", "topic-exchange", "topic-routingKey"),
    TOPIC_QUEUE1("topic-queue1", "topic-exchange", "topic-routingKey"),

    ;

    private final String queue;
    private final String exchange;
    private final String routingKey;

    MqEnums(String queue, String exchange, String routingKey) {
        this.queue = queue;
        this.exchange = exchange;
        this.routingKey = routingKey;
    }

}