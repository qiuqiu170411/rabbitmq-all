package com.example.springbootrabbitmq.service.backup;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * @author wwp
 * @version 1.0.0
 * @since 2021年07月20日 15:51:00
 */
@Slf4j
@Component
public class ReceiveConsumer {
    @RabbitListener(queues = "test.queue")
    public void receiveWarningMsg(Message message) {
        String msg = new String(message.getBody());
        log.info(">>> 接收到队列 test.queue 消息:{}", msg);
    }
}
