package com.example.springbootrabbitmq.service.backup;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * @author wwp
 * @version 1.0.0
 * @since 2021年07月20日 15:51:00
 */
@Slf4j
@Component
public class WarningConsumer {
    @RabbitListener(queues = "warning.queue")
    public void receiveMsg(Message message) {
        String msg = new String(message.getBody());
        log.error(">>> 报警发现不可路由消息： {}", msg);
    }
}
