package com.example.springbootrabbitmq.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class TopicQueueConsumer1 {

    @Value("${server.port}")
    private String port;

    @RabbitListener(queues = "topic-queue1")
    public void receiveMsg(Message message) {
        String msg = new String(message.getBody());
        System.err.println(">>>> port " + port);
        log.info(">>> 接收到队列 topic-queue1 消息:{}", msg);
    }

}