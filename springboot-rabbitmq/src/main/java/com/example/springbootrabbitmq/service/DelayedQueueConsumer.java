//package com.example.springbootrabbitmq.service;
//
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.amqp.core.Message;
//import org.springframework.amqp.rabbit.annotation.RabbitListener;
//import org.springframework.stereotype.Component;
//
//import java.io.IOException;
//import java.time.LocalDateTime;
//
///**
// * 监听插件实现的延迟队列
// */
//@Slf4j
//@Component
//public class DelayedQueueConsumer {
//
//    @RabbitListener(queues = "delayed.queue")
//    public void receiveMsg(Message message) throws IOException {
//        String msg = new String(message.getBody());
//        log.info(">>> 当前时间:[{}],收到死信队列信息[{}]", LocalDateTime.now(), msg);
//    }
//
//}