package com.example.springbootrabbitmq.config;

import com.example.springbootrabbitmq.enums.MqEnums;
import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 备份队列配置
 *
 * @author wwp
 * @version 1.0.0
 * @since 2021年07月20日 14:24:00
 */
@Configuration
public class BackupConfig {

    @Bean(value = "backupQueue")
    public Queue backupQueue() {
        return QueueBuilder.durable(MqEnums.ALTERNATE.getQueue()).build();
    }

    @Bean(value = "backupExchange")
    public FanoutExchange backupExchange() {
        return new FanoutExchange(MqEnums.ALTERNATE.getExchange(), true, false);
    }

    @Bean
    public Binding bindingBackupQueue(@Qualifier(value = "backupQueue") Queue backupQueue,
                                      @Qualifier(value = "backupExchange") FanoutExchange backupExchange) {
        return BindingBuilder.bind(backupQueue).to(backupExchange);
    }

    @Bean(value = "warningQueue")
    public Queue warningQueue() {
        return QueueBuilder.durable(MqEnums.WARNING.getQueue()).build();
    }

    @Bean
    public Binding bindingWarningQueue(@Qualifier(value = "warningQueue") Queue warningQueue,
                                       @Qualifier(value = "backupExchange") FanoutExchange backupExchange) {
        return BindingBuilder.bind(warningQueue).to(backupExchange);
    }

    @Bean(value = "testQueue")
    public Queue testQueue() {
        return QueueBuilder.durable(MqEnums.TEST.getQueue()).build();
    }

    @Bean(value = "testExchange")
    public DirectExchange testDirectExchange() {
        return ExchangeBuilder
                .directExchange(MqEnums.TEST.getExchange())
                .durable(true)
                //设置该交换机的备份交换机
                .withArgument("alternate-exchange", MqEnums.ALTERNATE.getExchange())
                .build();
    }

    @Bean
    public Binding bindingTestQueue(@Qualifier(value = "testQueue") Queue testQueue,
                                    @Qualifier(value = "testExchange") DirectExchange testDirectExchange) {
        return BindingBuilder.bind(testQueue).to(testDirectExchange).with(MqEnums.TEST.getRoutingKey());
    }

}
