package com.example.springbootrabbitmq.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.ReturnedMessage;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;

/**
 * @author wwp
 * @version 1.0.0
 * @since 2021年07月20日 11:16:00
 */
@Slf4j
@Component
public class RabbitmqReturnCallback implements RabbitTemplate.ReturnsCallback {

    @Override
    public void returnedMessage(ReturnedMessage returned) {
        log.error(" >>>消息[{}]" +
                        ",ID[{}]," +
                        "被交换机[{}]退回," +
                        "退回原因:[{}]," +
                        " 路由key:[{}]",
                new String(returned.getMessage().getBody()),
                returned.getMessage().getMessageProperties().getMessageId(),
                returned.getExchange(),
                returned.getReplyText(),
                returned.getRoutingKey()
        );
    }

}
