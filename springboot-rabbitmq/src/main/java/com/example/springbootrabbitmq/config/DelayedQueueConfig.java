package com.example.springbootrabbitmq.config;

import com.example.springbootrabbitmq.enums.MqEnums;
import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

/**
 * 结合延迟队列插件的 配置
 *
 * @author wwp
 * @version 1.0.0
 * @since 2021年07月19日 16:29:00
 */
@Configuration
public class DelayedQueueConfig {

//    @Bean(value = "delayedQueue")
//    public Queue delayedQueue() {
//        return new Queue(MqEnums.DELAYED.getQueue());
//    }
//
//    /**
//     * description: 自定义交换机 我们在这里定义的是一个延迟交换机
//     *
//     * @param delayedQueue : 延迟队列
//     * @return org.springframework.amqp.core.Exchange
//     * @author wwp
//     * @since 2021/7/19 16:40
//     **/
//    @Bean("delayedExchange")
//    public CustomExchange delayedExchange(@Qualifier(value = "delayedQueue") Queue delayedQueue) {
//        Map<String, Object> args = new HashMap<>();
//        //自定义交换机的类型
//        args.put("x-delayed-type", "direct");
//        // type 对应插件定义的延迟队列type
//        return new CustomExchange(MqEnums.DELAYED.getExchange(), "x-delayed-message", true, false, args);
//    }
//
//    @Bean
//    public Binding bindingDelayedQueue(@Qualifier("delayedQueue") Queue queue,
//                                       @Qualifier("delayedExchange") CustomExchange delayedExchange) {
//        return BindingBuilder.bind(queue).to(delayedExchange).with(MqEnums.DELAYED.getRoutingKey()).noargs();
//    }

}
