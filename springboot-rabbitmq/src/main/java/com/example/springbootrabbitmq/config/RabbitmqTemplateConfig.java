package com.example.springbootrabbitmq.config;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class RabbitmqTemplateConfig implements CommandLineRunner {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Override
    public void run(String... args) {
        initializationRabbitMqTemplateCallBack();
    }

    /**
     * 设置rabbitTemplate 全局的错误回调
     * <p>1.只要消息没到交换机,只会回调ConfirmCallback,到了交换机,ConfirmCallback和ReturnsCallback 都会回调</p>
     * <p>2.消息消费成功的话,会回调ConfirmCallback</p>
     * <p>3.回调ReturnsCallback的情况只有一种，那就是消息到了交换机，但是没找到队列，所以会回调ReturnsCallback</p>
     */
    private void initializationRabbitMqTemplateCallBack() {
        // 通过设置 mandatory 参数可以在当消息传递过程中不可达目的地时将消息返回给生产者
        rabbitTemplate.setMandatory(true);
        rabbitTemplate.setReturnsCallback(new RabbitmqReturnCallback());
        rabbitTemplate.setConfirmCallback(new RabbitmqTemplateConfirmCallback());
    }
}
