package com.example.springbootrabbitmq.config;

import com.example.springbootrabbitmq.enums.MqEnums;
import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

/**
 * @author wwp
 * @version 1.0.0
 * @since 2021年07月19日 10:53:00
 */
@Configuration
public class RabbitmqConfig {

    /**
     * 队列A、B 都是延迟队列 队列中的消息 到期会过期转入到死信队列
     * 队列C 不是延迟队列 但是发送到队列C中的消息 是过期消息 则到期无人消费 也会转入到死信队列
     */

    @Bean("normalAQueue")
    public Queue normalAQueue() {
        return QueueBuilder.durable(MqEnums.NORMAL_A.getQueue()).withArguments(buildDeadLetterParams(10000)).build();
    }

    @Bean("normalBQueue")
    public Queue normalBQueue() {
        return QueueBuilder.durable(MqEnums.NORMAL_B.getQueue()).withArguments(buildDeadLetterParams(40000)).build();
    }

    @Bean("normalCQueue")
    public Queue normalCQueue() {
        return QueueBuilder.durable(MqEnums.NORMAL_C.getQueue()).withArguments(buildDeadLetterParams()).build();
    }

    @Bean("normalExchange")
    public DirectExchange normalExchange() {
        return new DirectExchange(MqEnums.NORMAL_A.getExchange());
    }

    @Bean
    public Binding bindingNormalAQueue(@Qualifier("normalAQueue") Queue normalAQueue, @Qualifier("normalExchange") DirectExchange normalExchange) {
        return BindingBuilder.bind(normalAQueue).to(normalExchange).with(MqEnums.NORMAL_A.getRoutingKey());
    }

    @Bean
    public Binding bindingNormalBQueue(@Qualifier("normalBQueue") Queue normalBQueue, @Qualifier("normalExchange") DirectExchange normalExchange) {
        return BindingBuilder.bind(normalBQueue).to(normalExchange).with(MqEnums.NORMAL_B.getRoutingKey());
    }

    @Bean
    public Binding bindingNormalCQueue(@Qualifier("normalCQueue") Queue normalCQueue, @Qualifier("normalExchange") DirectExchange normalExchange) {
        return BindingBuilder.bind(normalCQueue).to(normalExchange).with(MqEnums.NORMAL_C.getRoutingKey());
    }

    /* 死信队列 */

    @Bean
    public Queue deadLetterQueue() {
        return new Queue(MqEnums.DEAD_LETTER.getQueue(), false, false, false);
    }

    @Bean
    public DirectExchange deadLetterExchange() {
        return new DirectExchange(MqEnums.DEAD_LETTER.getExchange(), false, false);
    }

    @Bean
    public Binding bindingDeadLetterQueue() {
        return BindingBuilder.bind(deadLetterQueue()).to(deadLetterExchange()).with(MqEnums.DEAD_LETTER.getRoutingKey());
    }

    /**
     * description: 构造连接死信队列的配置
     *
     * @param ttl : 过期时间 毫秒单位
     * @return java.util.Map<java.lang.String, java.lang.Object>
     * @author wwp
     * @since 2021/7/19 11:53
     **/
    public Map<String, Object> buildDeadLetterParams(long ttl) {
        Map<String, Object> params = new HashMap<>(2);
        //正常队列设置死信交换机 参数 key 是固定值
        params.put("x-dead-letter-exchange", MqEnums.DEAD_LETTER.getExchange());
        //正常队列设置死信 routing-key 参数 key 是固定值
        params.put("x-dead-letter-routing-key", MqEnums.DEAD_LETTER.getRoutingKey());
        //声明队列的 TTL
        params.put("x-message-ttl", ttl);
        return params;
    }

    public Map<String, Object> buildDeadLetterParams() {
        Map<String, Object> params = new HashMap<>(2);
        //正常队列设置死信交换机 参数 key 是固定值
        params.put("x-dead-letter-exchange", MqEnums.DEAD_LETTER.getExchange());
        //正常队列设置死信 routing-key 参数 key 是固定值
        params.put("x-dead-letter-routing-key", MqEnums.DEAD_LETTER.getRoutingKey());
        return params;
    }

}
