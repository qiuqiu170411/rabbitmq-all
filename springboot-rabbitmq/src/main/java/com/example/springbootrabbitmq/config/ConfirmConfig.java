package com.example.springbootrabbitmq.config;

import com.example.springbootrabbitmq.enums.MqEnums;
import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 配置发布确认队列 需要yml文件配置发布回调
 *
 * @author wwp
 * @version 1.0.0
 * @since 2021年07月20日 09:42:00
 */
@Configuration
public class ConfirmConfig {

    @Bean("confirmExchange")
    public DirectExchange confirmExchange() {
        return new DirectExchange(MqEnums.CONFIRM.getExchange());
    }

    @Bean("confirmQueue")
    public Queue confirmQueue() {
        return QueueBuilder.durable(MqEnums.CONFIRM.getQueue()).build();
    }

    @Bean
    public Binding queueBinding(@Qualifier("confirmQueue") Queue queue,
                                @Qualifier("confirmExchange") DirectExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with(MqEnums.CONFIRM.getRoutingKey());
    }

}
