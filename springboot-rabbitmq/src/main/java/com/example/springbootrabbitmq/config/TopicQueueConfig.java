package com.example.springbootrabbitmq.config;

import com.example.springbootrabbitmq.enums.MqEnums;
import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * topic队列
 *
 * @author weiweiping
 * @version 1.0
 * @since 2022/5/6 22:16
 */
@Configuration
public class TopicQueueConfig {

    @Bean("topicExchange")
    public TopicExchange confirmExchange() {
        return new TopicExchange(MqEnums.TOPIC_QUEUE.getExchange());
    }

    @Bean("topicQueue")
    public Queue topicQueue() {
        return QueueBuilder.durable(MqEnums.TOPIC_QUEUE.getQueue()).build();
    }

    @Bean("topicQueue1")
    public Queue topicQueue1() {
        return QueueBuilder.durable(MqEnums.TOPIC_QUEUE1.getQueue()).build();
    }

    @Bean("topicQueueBinding")
    public Binding queueBinding(@Qualifier("topicQueue") Queue queue,
                                @Qualifier("topicExchange") TopicExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with(MqEnums.TOPIC_QUEUE.getRoutingKey());
    }

    @Bean("topicQueueBinding1")
    public Binding queueBinding1(@Qualifier("topicQueue1") Queue queue,
                                @Qualifier("topicExchange") TopicExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with(MqEnums.TOPIC_QUEUE.getRoutingKey());
    }


}
