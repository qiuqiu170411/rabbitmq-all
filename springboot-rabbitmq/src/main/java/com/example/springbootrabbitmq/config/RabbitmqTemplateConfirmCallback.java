package com.example.springbootrabbitmq.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.ReturnedMessage;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;

/**
 * @author wwp
 * @version 1.0.0
 * @since 2021年07月20日 11:16:00
 */
@Slf4j
@Component
public class RabbitmqTemplateConfirmCallback implements RabbitTemplate.ConfirmCallback {

    @Override
    public void confirm(CorrelationData correlationData, boolean ack, String cause) {
        if (null != correlationData) {
            String id = correlationData.getId();
            ReturnedMessage returned = correlationData.getReturned();
            if (null != returned) {
                log.info(">>> 关联数据[{}], 确认状态[{}],交换机[{}],路由key[{}],因为原因[{}]: ", correlationData, ack, returned.getExchange(), returned.getRoutingKey(), cause);
            } else {
                log.info(">>> 关联数据[{}], 确认状态[{}],因为原因[{}]: ", correlationData, ack, cause);
            }
            if (ack) {
                log.info(">>> 交换机已经收到 id为:{}的消息", id);
            } else {
                log.info(">>> 交换机还未收到 id为:{}消息,由于原因:{}", id, cause);
            }
        }
    }

}
