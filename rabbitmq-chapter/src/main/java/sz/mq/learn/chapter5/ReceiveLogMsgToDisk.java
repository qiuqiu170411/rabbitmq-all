package sz.mq.learn.chapter5;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.CharsetUtil;
import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.CancelCallback;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DeliverCallback;
import sz.mq.learn.utils.RabbitmqUtils;

import java.io.File;
import java.nio.charset.StandardCharsets;

/**
 * Fanout 模式 接收日志消息 存储到磁盘文件中
 *
 * @author wwp
 * @version 1.0.0
 * @since 2021年07月14日 15:39:00
 */
public class ReceiveLogMsgToDisk {

    private static final String EXCHANGE_NAME = "test.fanout.change";

    public static void main(String[] args) throws Exception {
         /*
            1. 获取信道
            2. 声明交换机
            3. 声明队列
            4. 队列绑定交换机
            5. 从队列中取msg
            6. 将msg写入到磁盘
         */
        Channel channel = RabbitmqUtils.getChannel();
        channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.FANOUT);
        // 获取一个随机队列
        String queue = channel.queueDeclare().getQueue();
        channel.queueBind(queue, EXCHANGE_NAME, "");

        File file = new File("D:\\log.txt");
        DeliverCallback deliverCallback = (consumerTag, message) -> {
            String msg = new String(message.getBody(), StandardCharsets.UTF_8);
            System.out.println(msg);
            FileUtil.appendString("\r\n " + msg,file, CharsetUtil.CHARSET_UTF_8);
        };

        CancelCallback cancelCallback = x -> {
            // 取消消费的一个回调接口 如在消费的时候队列被删除掉了
        };
        channel.basicConsume(queue, deliverCallback, cancelCallback);
    }


}
