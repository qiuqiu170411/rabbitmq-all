package sz.mq.learn.chapter5;

import cn.hutool.core.util.RandomUtil;
import com.rabbitmq.client.Channel;
import sz.mq.learn.utils.RabbitmqUtils;

/**
 * 发送消息到交换机中
 *
 * @author wwp
 * @version 1.0.0
 * @since 2021年07月14日 15:39:00
 */
public class Producer {

    private static final String EXCHANGE_NAME = "test.fanout.change";

    public static void main(String[] args) throws Exception {

        Channel channel = RabbitmqUtils.getChannel();

        for (int i = 0; i < 100; i++) {
            channel.basicPublish(EXCHANGE_NAME, "", null, RandomUtil.randomString(15).getBytes());
        }

    }


}
