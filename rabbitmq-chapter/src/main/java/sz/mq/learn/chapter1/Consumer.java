package sz.mq.learn.chapter1;

import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * @author wwp
 * @version 1.0.0
 * @since 2021年07月09日 16:38:00
 */
public class Consumer {

    private static final String QUEUE = "HELLO";

    public static void main(String[] args) throws IOException, TimeoutException {
        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setHost("192.168.20.131");
        connectionFactory.setUsername("root");
        connectionFactory.setPassword("root");

        Connection connection = connectionFactory.newConnection();
        Channel channel = connection.createChannel();
        System.out.println(">>> 等待接收消息....");

        //推送的消息如何进行消费的接口回调
        DeliverCallback deliverCallback = (consumerTag, delivery) -> System.out.println(new String(delivery.getBody()));

        //取消消费的一个回调接口 如在消费的时候队列被删除掉了
        CancelCallback cancelCallback = (consumerTag) -> System.out.println(">>> 消息消费被中断");

        /*
         * 消费者消费消息
         * 1.消费哪个队列
         * 2.消费成功之后是否要自动应答 true 代表自动应答 false 手动应答
         * 3.消费者未成功消费的回调
         * 3.消费者取消消费的回调
         */
        channel.basicConsume(QUEUE, true, deliverCallback, cancelCallback);
    }

    /*
     问题：
     1. 消费者 autoAck 设置为false 会出现什么情况？
     2. 消费者 如何触发 deliverCallback回调？
     2. 消费者 如何触发 cancelCallback回调？
     */

    /*
        问题解答：
            1:  false 代表队列中没有消息的时候队列不会被删除
            2. 消费者 接收到消息的时候 回触发此回调
     */


}
