package sz.mq.learn.chapter1;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class Producer {

    private static final String QUEUE = "HELLO";

    public static void main(String[] args) throws IOException, TimeoutException {

        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setHost("192.168.20.131");
        connectionFactory.setUsername("root");
        connectionFactory.setPassword("root");

        Connection connection = connectionFactory.newConnection();

        Channel channel = connection.createChannel();

        /*
          生成一个队列
          1.队列名称
          2.队列里面的消息是否持久化 默认消息存储在内存中
          3.该队列是否只供一个消费者进行消费 是否进行共享 true 可以多个消费者消费
          4.是否自动删除 最后一个消费者端开连接以后 该队列是否自动删除 true 自动删除
          5.其他参数
         */
        channel.queueDeclare(QUEUE, false, false, true, null);

        channel.basicPublish("", QUEUE, null, "simple msg".getBytes());

    }

    /*
     问题：
     1. 发布者 未指定交换机时 是否有使用交换机发送消息？如果有,是使用的是系统默认的交换机吗？
     2. 创建的普通队列 没有设置routingKey 为什么推送消息的时候 routingKey 填写的是队列的名称 也能推送消息？
     */

    /*
        问题解答：
            每个队列会自动将一个和队列名一致的 Routing  Key 绑定到默认交换机上


        默认交换机
            默认交换机（default exchange）实际上是一个由RabbitMQ预先声明好的名字为空字符串的直连交换机（direct exchange）。
            它有一个特殊的属性使得它对于简单应用特别有用处：那就是每个新建队列（queue）都会自动绑定到默认交换机上，绑定的路由键（routing key）名称与队列名称相同。
            如：当你声明了一个名为”hello”的队列，RabbitMQ会自动将其绑定到默认交换机上，绑定（binding）的路由键名称也是为”hello”。
            因此，当携带着名为”hello”的路由键的消息被发送到默认交换机的时候，此消息会被默认交换机路由至名为”hello”的队列中。即默认交换机看起来貌似能够直接将消息投递给队列，如同我们之前文章里看到一例子。
     */
}
