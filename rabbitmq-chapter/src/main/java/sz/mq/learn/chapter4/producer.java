package sz.mq.learn.chapter4;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.date.TimeInterval;
import cn.hutool.core.lang.UUID;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.RandomUtil;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.ConfirmCallback;
import com.rabbitmq.client.MessageProperties;
import org.junit.Test;
import sz.mq.learn.utils.RabbitmqUtils;

import java.util.concurrent.ConcurrentNavigableMap;
import java.util.concurrent.ConcurrentSkipListMap;

/**
 * 发布确认 demo
 * 1. 单个确认
 * 2. 批量确认
 * 3. 异步批量确认
 *
 * @author wwp
 * @version 1.0.0
 * @since 2021年07月13日 09:34:00
 */
public class producer {

    private static final String QUEUE = IdUtil.randomUUID();

    /**
     * description: 正常发布消息 没有确认机制 耗时64ms左右
     *
     * @author wwp
     * @since 2021/7/13 11:07
     **/
    @Test
    public void normalConfirm() throws Exception {
        Channel channel = RabbitmqUtils.getChannel();
        channel.queueDeclare(QUEUE, false, false, false, null);
        TimeInterval timer = DateUtil.timer();
        for (int i = 0; i < 1050; i++) {
            String s = RandomUtil.randomNumbers(10);
            channel.basicPublish("", QUEUE, MessageProperties.PERSISTENT_TEXT_PLAIN, s.getBytes());
        }
        System.out.println("消息全部发送完毕！耗时[" + timer.intervalMs() + "]");
    }

    /**
     * description: 发布消息单个确认 耗时834ms左右
     *
     * @author wwp
     * @since 2021/7/13 11:07
     **/
    @Test
    public void singleConfirm() throws Exception {
        Channel channel = RabbitmqUtils.getChannel();
        channel.queueDeclare(QUEUE, false, false, false, null);
        // 设置发布确认
        channel.confirmSelect();
        TimeInterval timer = DateUtil.timer();
        for (int i = 0; i < 1050; i++) {
            String s = RandomUtil.randomNumbers(10);
            channel.basicPublish("", QUEUE, MessageProperties.PERSISTENT_TEXT_PLAIN, s.getBytes());
            boolean b = channel.waitForConfirms();
            if (b) {
                System.out.println("broker 确认消息成功！" + s);
            } else {
                System.out.println("broker 确认消息失败！");
            }
        }
        System.out.println("消息全部发送完毕！耗时[" + timer.intervalMs() + "]");
    }


    /**
     * description: 批量发布消息确认 耗时约106-110 ms
     *
     * @author wwp
     * @since 2021/7/13 11:28
     **/
    @Test
    public void batchConfirm() throws Exception {
        Channel channel = RabbitmqUtils.getChannel();
        channel.queueDeclare(QUEUE, false, false, false, null);
        // 设置发布确认
        channel.confirmSelect();

        int size = 5050;
        int batchSize = 100;
        int tempNum = 0;
        TimeInterval timer = DateUtil.timer();
        for (int i = 0; i < size; i++) {
            String s = RandomUtil.randomNumbers(10);
            channel.basicPublish("", QUEUE, MessageProperties.PERSISTENT_TEXT_PLAIN, s.getBytes());
            tempNum++;
            if (tempNum == batchSize) {
                boolean b = channel.waitForConfirms();
                if (b) {
                    System.out.println("broker 确认消息成功！" + s);
                } else {
                    System.out.println("broker 确认消息失败！");
                }
                tempNum = 0;
            }
        }

        if (tempNum > 0 & tempNum < 100) {
            System.out.println("还剩 + " + tempNum + "个消息待批量确认！");
            boolean b = channel.waitForConfirms();
            if (b) {
                System.out.println("broker 确认消息成功！");
            } else {
                System.out.println("broker 确认消息失败！");
            }
        }

        System.out.println("消息全部发送完毕！耗时[" + timer.intervalMs() + "]");
    }

    /**
     * description: 批量异步发布消息确认
     *
     * @author wwp
     * @since 2021/7/14 14:56
     **/
    @Test
    public void batchAsyncConfirm() throws Exception {
        Channel channel = RabbitmqUtils.getChannel();
        channel.queueDeclare(QUEUE, false, false, false, null);
        // 设置发布确认
        channel.confirmSelect();

        int size = 5050;
        TimeInterval timer = DateUtil.timer();

        ConfirmCallback ackCallback = (deliveryTag, multiple) -> {
            System.out.println("确认的消息");
        };

        ConfirmCallback nackCallback = (deliveryTag, multiple) -> {
            System.out.println("发送失败的消息" + deliveryTag);
        };

        channel.addConfirmListener(ackCallback, nackCallback);

        for (int i = 0; i < size; i++) {
            String s = RandomUtil.randomNumbers(10);
            channel.basicPublish("", QUEUE, MessageProperties.PERSISTENT_TEXT_PLAIN, s.getBytes());
        }
        System.out.println("消息全部发送完毕！耗时[" + timer.intervalMs() + "]");
    }


    /**
     * description: 批量异步发布消息确认 升级版
     *
     * @author wwp
     * @since 2021/7/14 14:56
     **/
    @Test
    public void batchAsyncConfirmExpand() throws Exception {
        Channel channel = RabbitmqUtils.getChannel();
        String queue = RandomUtil.randomString(3);
        // 声明队列
        channel.queueDeclare(queue, false, false, false, null);
        // 声明发布消息需要确认
        channel.confirmSelect();

        ConcurrentSkipListMap<Long, String> concurrentSkipListMap = new ConcurrentSkipListMap<>();

        TimeInterval timer = DateUtil.timer();
        ConfirmCallback ackCallback = (deliveryTag, multiple) -> {
            System.out.printf("ack回调已确认消息 序号[%s] msg[%s]", deliveryTag, concurrentSkipListMap.get(deliveryTag));
            if (multiple) {
                ConcurrentNavigableMap<Long, String> tempMap = concurrentSkipListMap.headMap(deliveryTag, true);
                tempMap.clear();
            } else {
                concurrentSkipListMap.remove(deliveryTag);
            }
        };
        ConfirmCallback nackCallback = (deliveryTag, multiple) -> {
            System.out.printf("未确认消息 序号[%s] msg[%s]", deliveryTag, concurrentSkipListMap.get(deliveryTag));
        };
        // 添加异步发布消息的监听器
        channel.addConfirmListener(ackCallback, nackCallback);

        for (int i = 0; i < 1000; i++) {
            String msg = RandomUtil.randomNumbers(5);
            long nextPublishSeqNo = channel.getNextPublishSeqNo();
            channel.basicPublish("", queue, null, msg.getBytes());
            concurrentSkipListMap.put(nextPublishSeqNo, msg);
            System.out.printf("发送消息 序号[%s] msg[%s]", msg, nextPublishSeqNo);
        }

        System.out.printf("耗时[%s]ms", timer.intervalMs());

    }


    @Test
    public void test1() throws Exception {
        try (Channel channel = RabbitmqUtils.getChannel()) {
            String queueName = UUID.randomUUID().toString();
            channel.queueDeclare(queueName, false, false, false, null);
            //开启发布确认
            channel.confirmSelect();
            /*
             * 线程安全有序的一个哈希表，适用于高并发的情况
             * 1.轻松的将序号与消息进行关联
             * 2.轻松批量删除条目 只要给到序列号
             * 3.支持并发访问
             */
            ConcurrentSkipListMap<Long, String> outstandingConfirms = new ConcurrentSkipListMap<>();
            /*
             * 确认收到消息的一个回调
             * 1.消息序列号
             * 2.true 可以确认小于等于当前序列号的消息
             * false 确认当前序列号消息
             */
            ConfirmCallback ackCallback = (sequenceNumber, multiple) -> {
                if (multiple) {
                    //删除已经确认的消息
                    ConcurrentNavigableMap<Long, String> confirmed = outstandingConfirms.headMap(sequenceNumber, true);
                    confirmed.clear();
                } else {
                    //只清除当前序列号的消息
                    outstandingConfirms.remove(sequenceNumber);
                }
            };
            ConfirmCallback nackCallback = (sequenceNumber, multiple) -> {
                String message = outstandingConfirms.get(sequenceNumber);
                System.out.println("发布的消息" + message + "未被确认，序列号" + sequenceNumber);
            };
            /*
             * 添加一个异步确认的监听器
             * 1.确认收到消息的回调
             * 2.未收到消息的回调
             */
            channel.addConfirmListener(ackCallback, nackCallback);
            long begin = System.currentTimeMillis();
            for (int i = 0; i < 5050; i++) {
                String message = "消息" + i;
                /*
                 * channel.getNextPublishSeqNo()获取下一个消息的序列号
                 * 通过序列号与消息体进行一个关联
                 * 全部都是未确认的消息体
                 */
                outstandingConfirms.put(channel.getNextPublishSeqNo(), message);
                channel.basicPublish("", queueName, null, message.getBytes());
            }
            long end = System.currentTimeMillis();
            System.out.println("发布" + 5050 + "个异步确认消息,耗时" + (end - begin) + "ms");
        }
    }

}
