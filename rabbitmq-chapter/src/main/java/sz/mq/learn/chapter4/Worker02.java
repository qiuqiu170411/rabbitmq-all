package sz.mq.learn.chapter4;

import com.rabbitmq.client.CancelCallback;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DeliverCallback;
import sz.mq.learn.utils.RabbitmqUtils;

public class Worker02 {
    private static final String QUEUE = "HELLO";

    public static void main(String[] args) throws Exception {
        Channel channel = RabbitmqUtils.getChannel();

        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            String receivedMessage = new String(delivery.getBody());
            System.out.println("接收到消息:" + receivedMessage);
        };

        CancelCallback cancelCallback = (consumerTag) -> System.out.println(consumerTag + "消费者取消消费接口回调逻辑");

        System.out.println("C2 消费者启动等待消费......");
        channel.basicConsume(QUEUE, true, deliverCallback, cancelCallback);
    }

}