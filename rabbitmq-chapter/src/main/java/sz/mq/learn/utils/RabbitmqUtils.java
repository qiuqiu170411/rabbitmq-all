package sz.mq.learn.utils;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

public class RabbitmqUtils {

    /**
     * description: 得到一个连接的 channel
     *
     * @return com.rabbitmq.client.Channel
     * @author wwp
     * @since 2021/7/11 21:02
     **/
    public static Channel getChannel() throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("192.168.20.131");
        factory.setUsername("root");
        factory.setPassword("root");
        Connection connection = factory.newConnection();
        return connection.createChannel();
    }

}
