package sz.mq.learn.chapter8.ttl;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.CancelCallback;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DeliverCallback;
import sz.mq.learn.utils.RabbitmqUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * @author wwp
 * @version 1.0.0
 * @since 2021年07月16日 10:10:00
 */
public class Consumer {

    private static final String NORMAL_EXCHANGE = "normal_exchange";
    private static final String DEAD_EXCHANGE = "dead_exchange";

    public static void main(String[] args) throws Exception {
        Channel channel = RabbitmqUtils.getChannel();

        channel.exchangeDeclare(NORMAL_EXCHANGE, BuiltinExchangeType.DIRECT);
        channel.exchangeDeclare(DEAD_EXCHANGE, BuiltinExchangeType.DIRECT);
        // 声明死信队列
        String deadQueue = "dead-queue";
        channel.queueDeclare(deadQueue, false, false, false, null);
        channel.queueBind(deadQueue, DEAD_EXCHANGE, "no");

        // 正常队列绑定死信队列
        String normalQueue = "normal-queue";
        Map<String, Object> params = new HashMap<>();
        //正常队列设置死信交换机 参数 key 是固定值
        params.put("x-dead-letter-exchange", DEAD_EXCHANGE);
        //正常队列设置死信 routing-key 参数 key 是固定值
        params.put("x-dead-letter-routing-key", "no");

        channel.queueDeclare(normalQueue, false, false, false, params);
        channel.queueBind(normalQueue, NORMAL_EXCHANGE, "ok");
        DeliverCallback deliverCallback = (consumerTag, message) -> {
            System.out.println("收到消息" + new String(message.getBody()));
        };
        CancelCallback cancelCallback = consumerTag -> {

        };
        channel.basicConsume(normalQueue, deliverCallback, cancelCallback);


    }

}
