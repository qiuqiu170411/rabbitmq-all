package sz.mq.learn.chapter8.length;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DeliverCallback;
import sz.mq.learn.utils.RabbitmqUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * @author wwp
 * @version 1.0.0
 * @since 2021年07月16日 10:10:00
 */
public class Consumer1 {

    private static final String dead_letter_exchange = "dead_letter_exchange";
    private static final String dead_letter_routing_key = "dead_letter_routing_key";

    private static final String normal_exchange = "normal_exchange";
    private static final String normal_routingKey = "normal_routingKey";

    public static void main(String[] args) throws Exception {
        /*
            1. 定义死信队列 并绑定交换机
            2. 定义接收消息的队列,并绑定交换机,绑定死信队列
            3. 接收消息
            4. 关闭消费者 停止接收消息 让消息过期进入到死信队列
         */
        //Dead letter
        String deadQueue = "dead_queue";

        Channel channel = RabbitmqUtils.getChannel();
        channel.exchangeDeclare(normal_exchange, BuiltinExchangeType.DIRECT);
        channel.exchangeDeclare(dead_letter_exchange, BuiltinExchangeType.DIRECT);

        channel.queueDeclare(deadQueue, false, false, false, null);
        channel.queueBind(deadQueue, dead_letter_exchange, dead_letter_routing_key);


        Map<String, Object> arguments = new HashMap<>();
        //正常队列设置死信交换机 参数 key 是固定值
        arguments.put("x-dead-letter-exchange", dead_letter_exchange);
        //正常队列设置死信 routing-key 参数 key 是固定值
        arguments.put("x-dead-letter-routing-key", dead_letter_routing_key);
        //设置队列最多接收6个消息
        arguments.put("x-max-length", 6);

        String normalQueue = "normal_queue";
        channel.queueDeclare(normalQueue, false, false, false, arguments);
        channel.queueBind(normalQueue, normal_exchange, normal_routingKey);

        DeliverCallback deliverCallback = (consumerTag, message) -> {
            System.out.println("接收到的消息" + new String(message.getBody()));
        };
        channel.basicConsume(normalQueue, deliverCallback, consumerTag -> {

        });
    }

}
