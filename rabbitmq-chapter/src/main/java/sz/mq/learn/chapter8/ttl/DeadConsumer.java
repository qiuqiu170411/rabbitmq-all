package sz.mq.learn.chapter8.ttl;

import com.rabbitmq.client.CancelCallback;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DeliverCallback;
import sz.mq.learn.utils.RabbitmqUtils;

/**
 * @author wwp
 * @version 1.0.0
 * @since 2021年07月16日 10:36:00
 */
public class DeadConsumer {

    private static final String DEAD_EXCHANGE = "dead_exchange";

    public static void main(String[] args) throws Exception {
        Channel channel = RabbitmqUtils.getChannel();
        String deadQueue = "dead-queue";
        channel.queueDeclare(deadQueue, false, false, false, null);
        channel.queueBind(deadQueue, DEAD_EXCHANGE, "no");

        DeliverCallback deliverCallback = (consumerTag, message) -> {
            System.out.println("Consumer02 接收死信队列的消息" + new String(message.getBody()));
        };
        CancelCallback cancelCallback = consumerTag -> {

        };
        channel.basicConsume(deadQueue, deliverCallback, cancelCallback);
    }

}
