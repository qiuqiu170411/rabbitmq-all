package sz.mq.learn.chapter8.length;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import sz.mq.learn.utils.RabbitmqUtils;

import java.nio.charset.StandardCharsets;
import java.util.Scanner;

/**
 * 生产者发送消息 到队列
 * 消息过期后 转入到死信队列
 *
 * @author wwp
 * @version 1.0.0
 * @since 2021年07月16日 11:11:00
 */
public class Producer1 {

    private static final String normal_exchange = "normal_exchange";
    private static final String normal_routingKey = "normal_routingKey";

    public static void main(String[] args) throws Exception {
        /*
            1. 定义交换机.
            2. 发送有过期时间限制的消息到交换机.
         */
        Channel channel = RabbitmqUtils.getChannel();
        channel.exchangeDeclare(normal_exchange, BuiltinExchangeType.DIRECT);

        AMQP.BasicProperties properties = new AMQP.BasicProperties().builder().expiration("10000").build();
        Scanner sc = new Scanner(System.in);
        while (true){
            System.out.println("请输入要发送的消息:");
            String msg = sc.nextLine();
            channel.basicPublish(normal_exchange, normal_routingKey, properties, msg.getBytes(StandardCharsets.UTF_8));
        }
    }


}
