package sz.mq.learn.chapter8.length;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DeliverCallback;
import sz.mq.learn.utils.RabbitmqUtils;

/**
 * @author wwp
 * @version 1.0.0
 * @since 2021年07月16日 10:10:00
 */
public class DeadConsumer1 {

    private static final String dead_letter_exchange = "dead_letter_exchange";
    private static final String dead_letter_routing_key = "dead_letter_routing_key";

    public static void main(String[] args) throws Exception {
        /*
            1. 定义死信队列 并绑定交换机
            2. 接收死信消息 打印在控制台
         */
        Channel channel = RabbitmqUtils.getChannel();
        String deadQueue = "dead_queue";
        channel.queueDeclare(deadQueue, false, false, false, null);
        channel.queueBind(deadQueue, dead_letter_exchange, dead_letter_routing_key);

        DeliverCallback deliverCallback = (consumerTag, message) -> {
            System.out.println("接收到死信队列消息:" + new String(message.getBody()));
        };
        channel.basicConsume(deadQueue, deliverCallback,consumerTag -> {});
    }

}
