package sz.mq.learn.chapter8.ttl;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import sz.mq.learn.utils.RabbitmqUtils;

import java.util.UUID;

/**
 * @author wwp
 * @version 1.0.0
 * @since 2021年07月16日 09:29:00
 */
public class Producer {

    private static final String NORMAL_EXCHANGE = "normal_exchange";

    public static void main(String[] args) throws Exception {
        // 设置过期
        Channel channel = RabbitmqUtils.getChannel();
        channel.exchangeDeclare(NORMAL_EXCHANGE, BuiltinExchangeType.DIRECT);
        AMQP.BasicProperties properties = new AMQP.BasicProperties.Builder().expiration("10000").build();
        for (int i = 0; i < 10; i++) {
            String msg = UUID.randomUUID().toString();
            channel.basicPublish(NORMAL_EXCHANGE, "ok", properties, msg.getBytes());
            System.out.println("生产者发送消息:" + msg);
        }
    }

}
