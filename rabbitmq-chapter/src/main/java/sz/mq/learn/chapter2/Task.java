package sz.mq.learn.chapter2;

import com.rabbitmq.client.Channel;
import sz.mq.learn.utils.RabbitmqUtils;

import java.nio.charset.StandardCharsets;
import java.util.Scanner;

public class Task {

    private static final String QUEUE_NAME = "hello";

    public static void main(String[] args) throws Exception {

        Channel channel = RabbitmqUtils.getChannel();

        channel.queueDeclare(QUEUE_NAME, false, false, false, null);

        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("请输入要发送的消息：");
            channel.basicPublish("", QUEUE_NAME, null, scanner.nextLine().getBytes(StandardCharsets.UTF_8));
        }
    }

}
