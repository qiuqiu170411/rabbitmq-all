package sz.mq.learn.chapter3;

import cn.hutool.core.thread.ThreadUtil;
import com.rabbitmq.client.CancelCallback;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DeliverCallback;
import sz.mq.learn.utils.RabbitmqUtils;

public class Worker01 {
    private static final String QUEUE_NAME = "hello";

    public static void main(String[] args) throws Exception {
        Channel channel = RabbitmqUtils.getChannel();

        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            String receivedMessage = new String(delivery.getBody());
            System.out.println("接收到消息:" + receivedMessage);
            // false代表不批量消费（消费一个 确认一个）
            ThreadUtil.sleep(1000 * 15);
            channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
            System.out.println("C1 确认接收消息");
        };

        CancelCallback cancelCallback = (consumerTag) -> System.out.println(consumerTag + "消费者取消消费接口回调逻辑");

        System.out.println("C1 消费者启动等待消费......");
        boolean autoAck = false;
        channel.basicConsume(QUEUE_NAME, autoAck, deliverCallback, cancelCallback);
    }

}