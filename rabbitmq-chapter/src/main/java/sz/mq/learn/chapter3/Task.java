package sz.mq.learn.chapter3;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.MessageProperties;
import sz.mq.learn.utils.RabbitmqUtils;

import java.nio.charset.StandardCharsets;
import java.util.Scanner;

public class Task {

    private static final String QUEUE_NAME = "hello";

    public static void main(String[] args) throws Exception {

        Channel channel = RabbitmqUtils.getChannel();

        channel.queueDeclare(QUEUE_NAME, false, false, false, null);

        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("请输入要发送的消息：");
//            channel.basicPublish("", QUEUE_NAME, null, scanner.nextLine().getBytes(StandardCharsets.UTF_8));
            // 简单 消息设置持久化 如果服务在消息持久化之前宕机还是会丢失消息
            channel.basicPublish("", QUEUE_NAME, MessageProperties.PERSISTENT_TEXT_PLAIN, scanner.nextLine().getBytes(StandardCharsets.UTF_8));
        }
    }

}
