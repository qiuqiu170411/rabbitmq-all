package sz.mq.learn.chapter6;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DeliverCallback;
import sz.mq.learn.utils.RabbitmqUtils;

/**
 * @author wwp
 * @version 1.0.0
 * @since 2021年07月14日 17:10:00
 */
public class ReceiveAllLog {

    private static final String EXCHANGE_NAME = "test.direct.change";

    public static void main(String[] args) throws Exception {

        Channel channel = RabbitmqUtils.getChannel();
        String queue = channel.queueDeclare().getQueue();
        channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.DIRECT);
        channel.queueBind(queue, EXCHANGE_NAME, "error");
        channel.queueBind(queue, EXCHANGE_NAME, "info");

        DeliverCallback deliverCallback = (consumerTag, message) -> System.out.println(new String(message.getBody()));

        channel.basicConsume(queue, deliverCallback, x -> {
        });

    }

}
