package sz.mq.learn.chapter6;

import cn.hutool.core.util.RandomUtil;
import com.rabbitmq.client.Channel;
import sz.mq.learn.utils.RabbitmqUtils;

import java.util.Scanner;

/**
 * 发送消息到交换机中
 *
 * @author wwp
 * @version 1.0.0
 * @since 2021年07月14日 15:39:00
 */
public class Producer {

    private static final String EXCHANGE_NAME = "test.direct.change";

    public static void main(String[] args) throws Exception {

        Channel channel = RabbitmqUtils.getChannel();
//        channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.DIRECT);
        Scanner sc = new Scanner(System.in);
        String msg;
        for (; ; ) {
            System.out.println("请输入要发送的消息:");
            msg = sc.nextLine();
            try {
                Integer.parseInt(msg.charAt(0) + "");
            } catch (Exception e) {
                msg = RandomUtil.randomNumbers(1) + msg;
                System.out.println("转换失败");
                e.printStackTrace();
            }
            channel.basicPublish(EXCHANGE_NAME, msg.charAt(0) % 2 == 0 ? "info" : "error", null, RandomUtil.randomString(15).getBytes());
        }
    }

}
